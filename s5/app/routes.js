const { exchangeRates } = require('../src/util.js');

module.exports = (app) => {
	app.get('/', (req, res) => {
		return res.send({'data': {} });
	});

	app.get('/rates', (req, res) => {
		return res.send({
			rates: exchangeRates
		});
	})

	app.post('/currency', (req, res) => {

        if(!req.body.hasOwnProperty('name')){
            return res.status(400).send({
                'error': 'Bad Request - missing required parameter NAME'
            })
        }


        if (req.body){
        	console.log("req.body = "+ req.body);
        	Object.keys(req.body).forEach((alias)=>{
        		if (Object.keys(req.body[alias]).indexOf('ex') >=0){
	        		if (Object.keys(req.body[alias]['ex']).indexOf(alias) != -1){	 
		            	console.log ("duplicate alias found") ;          		
					    return res.status(400).send({
					        'error': 'Bad Request - duplicate ALIAS found'
					    });
		        	}
        		}
	        })
	    }

        // if (req.body){
        // 	console.log("req.body = "+ req.body);
        // 	Object.keys(req.body).forEach((alias)=>{
        // 		if (Object.keys(req.body[alias]).indexOf('ex') >=0){
	    //     		if (Object.keys(req.body[alias]['ex']).indexOf(alias) > -1){	 
		//             	console.log ("duplicate alias found") ;          		
		// 			    return res.status(200).send({
		// 			        'success': 'No duplicate ALIAS found'
		// 			    });
		//         	}
        // 		}
	    //     })
	    // }







        if(typeof req.body.name !== 'string'){
            return res.status(400).send({
                'error': 'Bad Request - NAME has to be a string'
            })
        }

        if(req.body.name.length === 0){
            return res.status(400).send({
                'error': 'Bad Request - NAME is empty'
            })
        }

        if(!req.body.hasOwnProperty('ex')){
            return res.status(400).send({
                'error': 'Bad Request - missing required parameter EX'
            })
        }

        if(typeof req.body.ex !== 'object'){
            return res.status(400).send({
                'error': 'Bad Request - EX has to be an object'
            })
        }

        if(Object.keys(req.body.ex).length === 0){
            return res.status(400).send({
                'error': 'Bad Request - EX is empty'
            })
        }

        if (!req.body.some(element => typeof element !== "object")) {
             return res.status(400).send({
                'error': 'Bad Request - ALIAS is missing'
            })
        }

		if (!Object.keys(req.body).every(key => typeof key === "string")) {
		    return res.status(400).send({
		        'error': 'Bad Request - Some alias are not strings'
		    });
		}

        if (Object.keys(req.body).length === 0) {
             return res.status(400).send({
                'error': 'Bad Request - ALIAS is empty'
            })
        }

        if (req.body){
        	console.log("testing phase")
        	Object.keys(req.body).forEach((alias)=>{
	            if (exchangeRates[alias].ex[alias]){
	            	return res.status(400).send({
		        		'error': 'Bad Request - duplicate ALIAS found'
		    		})
	        	}
	        })
	        return res.status(200);
	    }

	    return res.status(200).send({
	         'message': 'Request successful - no duplicates found'
	    });

    })

}
