const chai = require('chai');
const expect = chai.expect;
const http = require('chai-http');
chai.use(http);

describe('forex_api_test_suite', () => {
	
	it('test_api_get_rates_is_running', () => {
		chai.request('http://localhost:5001').get('/rates')
		.end((err, res) => {
			expect(res).to.not.equal(undefined);
		})
	})
	
	it('test_api_get_rates_returns_200', (done) => {
		chai.request('http://localhost:5001')
		.get('/rates')
		.end((err, res) => {
			expect(res.status).to.equal(200);
			done();	
		})		
	})
	
	it('test_api_get_rates_returns_object_of_size_5', (done) => {
		chai.request('http://localhost:5001')
		.get('/rates')
		.end((err, res) => {
			expect(Object.keys(res.body.rates)).does.have.length(5);
			done();	
		})		
	})
})	
describe('forex_api_test_suite_currency', () => {	
	it("test_api_currency_is_running", () => {
		chai.request("http://localhost:5001")
			.get('/currency')
			.end((err, res) => {
				expect(res).to.not.equal(undefined);
			})
	})

	it('test_api_post_currency_returns_400_if_name_is_missing', (done) => {
		chai.request('http://localhost:5001')
			.post('/currency')
			.type('json')
			.send({
				ex: "123"
			})
			.end((err, res) => {
				expect(res.status).to.equal(400)
				done();
			})
	})

	it('test_api_post_currency_returns_400_if_name_is_not_string', (done) => {
		chai.request('http://localhost:5001')
			.post('/currency')
			.type('json')
			.send({
				name: 123
			})
			.end((err, res) => {
				expect(res.status).to.equal(400)
				done();
			})
	})

	it('test_api_post_currency_returns_400_if_name_is_empty', (done) => {
		chai.request('http://localhost:5001')
			.post('/currency')
			.type('json')
			.send({
				name: ""
			})
			.end((err, res) => {
				expect(res.status).to.equal(400)
				done();
			})
	})


	it('test_api_post_currency_returns_400_if_ex_is_missing', (done) => {
		chai.request('http://localhost:5001')
			.post('/currency')
			.type('json')
			.send({
				
			})
			.end((err, res) => {
				expect(res.status).to.equal(400)
				done();
			})
	})

	it('test_api_post_currency_returns_400_if_ex_is_not_object', (done) => {
		chai.request('http://localhost:5001')
			.post('/currency')
			.type('json')
			.send({
				ex: "123"
			})
			.end((err, res) => {
				expect(res.status).to.equal(400)
				done();
			})
	})

	it('test_api_post_currency_returns_400_if_ex_is_empty', (done) => {
		chai.request('http://localhost:5001')
			.post('/currency')
			.type('json')
			.send({
				ex:{} 
			})
			.end((err, res) => {
				expect(res.status).to.equal(400)
				done();
			})
	})

	it('test_api_post_currency_returns_400_if_alias_is_missing', (done) => {
	    chai.request('http://localhost:5001')
	        .post('/currency')
	        .type('json')
	        .send([
	            {"not": "an alias"}
	        ])
	        .end((err, res) => {
	            expect(res.status).to.equal(400);
	            done();
	        });
	});

	it('test_api_post_currency_returns_400_if_alias_not_strings', (done) => {
	    chai.request('http://localhost:5001')
	        .post('/currency')
	        .type('json')
	        .send({
	            "alias1": "value1",
	            "alias2": "value2",
	            notAString: "value3" 
	        })
	        .end((err, res) => {
	            expect(res.status).to.equal(400);
	            done();
	        });
	});

	it('test_api_post_currency_returns_400_if_alias_is_empty', (done) => {
	    chai.request('http://localhost:5001')
	        .post('/currency')
	        .type('json')
	        .send([

	        ])
	        .end((err, res) => {
	            expect(res.status).to.equal(400);
	            done();
	        });
	});



	it('test_api_post_currency_returns_400_if_complete_field_duplicate_alias', (done) => {
	    chai.request('http://localhost:5001')
	        .post('/currency')
	        .type('json')
	        .send({
	            usd: {
	                name: 'United States Dollar',
	                ex: {
	                    peso: 50.73,
	                    won: 1187.24,
	                    yen: 108.63,
	                    usd: 7.03
	                }
	            }
	        })
	        .end((err, res) => {
	            expect(res.status).to.equal(400);
	            done();

	        });
	});



	it('test_api_post_currency_returns_200_if_complete_field_no_duplicates', (done) => {
	    chai.request('http://localhost:5001')
	        .post('/currency')
	        .type('json')
	        .send({
	            usd: {
	                name: 'United States Dollar',
	                ex: {
	                    peso: 50.73,
	                    won: 1187.24,
	                    yen: 108.63,
	                    yuan: 7.03
	                }
	            }
	        })
	        .end((err, res) => {
	            expect(res.status).to.equal(400);
	            done();

	        });
	});



})
