const { exchangeRates } = require('../src/util.js');

module.exports = (app) => {
	app.get('/', (req, res) => {
		return res.send({'data': {} });
	});

	app.get('/rates', (req, res) => {
		return res.send({
			rates: exchangeRates
		});
	})
}
