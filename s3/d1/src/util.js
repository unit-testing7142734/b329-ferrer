function factorial(n) {
    if(n === 0) return 1;
    if(n === 1) return 1;
    return n * factorial(n - 1);
}

function div_check(n) {
    if(n % 5 === 0) return true;
    if(n % 7 === 0) return true;
    return false;
}

const names = {
    "Brandon" : {
        "name": "Brandon Boyd",
        "age": 35        
    },
    "Steve" : {
        "name": "Steve Tyler",
        "age": 56        
    }
}




module.exports = {
    factorial: factorial,
    div_check: div_check,
    names: names
};