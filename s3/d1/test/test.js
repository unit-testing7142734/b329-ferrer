const {factorial,div_check} = require("../src/util.js");

// gets the expect and assert function from chai
//https://www.chaijs.com/api/assert/
//https://www.chaijs.com/api/bdd/
const {expect, assert} = require("chai");

//Test Suites are made up of collection of trst cases that should be executed together

//"describe()" keyword is used to group test together
describe('test_fun_factorials', ()=>{
	//"it() isused to define a single test case"
	//"it()" accepts 2 parameters
	// a string explaining what the test should do
	// callback function which contains the aactual test

	it('test_fun_factorial_5!_is_120', () => {
		const product = factorial (5);
		expect(product).to.equal(120);
	});
	
	it('test_fun_factorial_1!_is_1', () => {
		const product = factorial (1);
		expect(product).to.equal(1);
	});

	it('test_fun_factorial_0!_is_1', () => {
		const product = factorial (0);
		expect(product).to.equal(1);
	});
	
	it('test_fun_factorial_4!_is_24', () => {
		const product = factorial (4);
		expect(product).to.equal(24);
	});

	it('test_fun_factorial_10!_is_3628800', () => {
		const product = factorial (10);
		expect(product).to.equal(3628800);
	});


	// it('test_fun_factorial_1!_is_1', () => {
	// 	const product = factorial (1);
	// 	assert.equal(product, 1);
	// });

});

describe('test_divisibility_by_5_or_7', () => {
    it('test_100_is_divisible_by_5_or_7', () => {
        const divisible = div_check (100);
        assert.equal(divisible, true);
    });

    it('test_49_is_divisible_by_5_or_7', () => {
        const divisible = div_check (49);
        assert.equal(divisible, true);
    });

    it('test_30_is_divisible_by_5_or_7', () => {
        const divisible = div_check (30);
        assert.equal(divisible, true);
    });

    it('test_56_is_divisible_by_5_or_7', () => {
        const divisible = div_check (56);
        assert.equal(divisible, true);
    });
});

//s03 activity
const chai = require("chai");
const http = require("chai-http");
chai.use(http);

describe("api_test_suite_users", ()=>{

	it("test_api_users_is_running", () =>{
		chai.request("http://localhost:5001")
			.get("/users")
			.end((err,res) => {
				expect(res).to.not.equal(undefined);
			})
	})

	it("test_api_post_user_return_400_if_no_username", (done) =>{
		chai.request("http://localhost:5001")
			.post("/users")			
			.type('json')
			.send({
				age:28
			})
			.end((err,res) => {
				// "400" means error as defined in routes.js
				expect(res.status).to.equal(400);
				done();
			})
	})

	it("test_api_post_user_return_400_if_no_age", (done) =>{
		chai.request("http://localhost:5001")
			.post("/users")			
			.type('json')
			.send({
				username: "Bob Sagat"			
			})
			.end((err,res) => {
				// "400" means error as defined in routes.js
				expect(res.status).to.equal(400);
				done();
			})
	})


});