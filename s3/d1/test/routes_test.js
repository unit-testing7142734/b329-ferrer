const chai = require("chai");

const expect  = chai.expect;
// same as const {expect}  = require("chai")";

const http = require("chai-http");
chai.use(http);

describe("api_test_suite", ()=>{

	it("test_api_people_is_running", () =>{
		//documentation https://www.chaijs.com/api/bdd/
		chai.request("http://localhost:5001")
			//".get()" specifies the type of http request as GET and accept the API endpoint 
			.get("/people")
			//".end()" is the method that handles the response and error that will be recieved from the endpoint
			.end((err,res) => {
				// ".not" negates all assertions that follow in the chain
				expect(res).to.not.equal(undefined);
			})
	})


	it("test_api_people_return_200", (done) =>{
		chai.request("http://localhost:5001")
			.get("/people")
			.end((err,res) => {
				// "200" means success
				expect(res.status).to.equal(200);
				// "done()" function is typicallly used in asynchronous test to signal that the case is complete
				done();
			})
	})

	it("test_api_post_person_return_400_if_no_person_name", (done) =>{
		chai.request("http://localhost:5001")
			.post("/person")
			// ".type()" specifies the type of the input to be sent out as part of the POST request
			.type('json')
			// ".send ()" specifies the data to be sent as part of POST request
			//we used "alias" instead of "name" to invoke error
			.send({
				alias:"Jason", 
				age:28
			})
			.end((err,res) => {
				// "400" means error as defined in routes.js
				expect(res.status).to.equal(400);
				done();
			})
	})


})









