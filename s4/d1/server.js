const express = require ("express");
const app = express();
const PORT = 5001;


//middleware
app.use(express.json());


require("./app/routes")(app,{});

//Listener
app.listen(PORT, ()=>{
	console.log('running on port'  + PORT);
})